TP : API d’écriture et de modification d’un fichier cities.json

ROUTES :
GET /cities : Retourne le contenu du fichier cities.json

POST /city {'name' : cityName} : 
    - Créer le fichier cities.json s'il n'existe pas
    - Insère une nouvelle ville dans cities.json avec un id généré par uuid

PUT /city {'name' : cityName, 'id' : cityId} :
    - Met à jour la ville

DELETE /city/:id : Supprime la ville dont l'id est spécifié dans l'URL    



Project content :
- Folder TP_TRAN : Code
- PDF Screenshot des requêtes Postman : doc avec des screenshots des requests postman


Remarque : Les screens des cas d'erreur ne correpond plus au résultat final actuel car il a été ajouté un loggueur.


Angelo TRAN