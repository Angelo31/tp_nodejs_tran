#!/usr/bin/env node

'use strict';

const fs = require('fs');
const uuid = require('uuid');
const express = require('express');
const bodyParser = require('body-parser');

const port = 3000;

const app = express();

app.use(express.json());

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.set('view engine', 'pug');
//app.use(express.static(path.join(__dirname, 'public')));

const fileName = 'cities.json';


// ROUTE GET CITIES => RETURN cities.json's file content
app.get('/cities', (req, res, next) => {
    fs.readFile(fileName, 'utf8', (err, data) => {
        if(err){
            console.error(err);
            res.statusCode = 500;
            next(new Error("Can't find cities' file!"));
        } else {
            const myData = JSON.parse(data);
            res.render('index', {title : 'Voici le contenue du fichier', data : myData['cities']});
            res.statusCode = 200;
        }
    });
});


// ROUTE POST CITY => Add the city specified in the body
app.post('/city', (req, res, next) => {
    if(req.body && req.body['name']){
        const newCity = req.body['name'];
        const newId = uuid.v1();  
        
        fs.readFile(fileName, 'utf8', (err, data) => {
            if(err){
                fs.writeFile(fileName, "{\"cities\" : [{\"name\" : \""+newCity +"\", \"id\" : \""+ newId +"\"}]}", err => {
                    if (err){
                        res.statusCode = 500;
                        next(new Error("Serveur error"));
                    } else {
                        res.status(200).send("<h1>City " + newCity + "((<i>"+ newId + "</i>) added! (+ file created)</h1> ");
                    }
                });
            } else {
                const myData = JSON.parse(data);
                let alreadyExists = false;

                for (var i = 0; i < myData['cities'].length; i++) {
                    if (myData['cities'][i]['name'] === newCity){
                        alreadyExists = true;
                    }
                }
                if(alreadyExists){
                    res.statusCode = 400;
                    next(new Error("City (" + newCity + ") already exists"));
                } else {
                    const newData = {'id' : newId, 'name' : newCity};
                    myData['cities'].push(newData);
                    fs.writeFile(fileName,JSON.stringify(myData), err =>{ res.statusCode = 500; next(new Error("Serveur error"));});         
                    res.status(200).send("<h1>City " + newCity + "(<i>"+ newId + "</i>) added!</h1> ");
                    console.log("INSERT : id =>" + newId + " name => " + newCity);
                }
            }
        });
    } else {
        res.statusCode = 500;
        next(new Error("Empty Body / Invalid Body : FORMAT => { 'name' : <INPUT_CITY_ID> }"));
    }
});


// ROUTE PUT CITY => Update the city specified in the body
app.put('/city', (req, res, next) => {
    if(req.body && req.body['id'] && req.body['name']){
        fs.readFile(fileName, 'utf8', (err, data) => {
            if(err){
                console.error(err);
                res.statusCode = 500;
                next(new Error("Can't find cities' file!"));
            } else {
                const myData = JSON.parse(data);
                const newId = req.body['id'];
                const newCity = req.body['name']; 
                let isInsertAble = true;
                let isUpdated = false;
                
                for (var i = 0; i < myData['cities'].length; i++) {
                    if (myData['cities'][i]['name'] === newCity){
                        isInsertAble = false;
                    }
                }
                if(isInsertAble){
                    for (var i = 0; i < myData['cities'].length; i++) {
                        if (myData['cities'][i]['id'] === newId){
                            myData['cities'][i]['name'] = newCity;
                            isUpdated = true;
                        }
                    }
                    
                    if(isUpdated){
                        fs.writeFile(fileName,JSON.stringify(myData), err =>{ res.statusCode = 500;});             
                        res.status(200).send("<h1>City " + newCity + "((<i>"+ newId + "</i>) updated!</h1> ");
                        console.log("UPDATE : id => " + newId + " name => " + newCity)
                    } else {
                        res.statusCode = 404;
                        next(new Error("Can't find the city specified!("+ newCity + ")"));
                    }
                } else {
                    res.statusCode = 404;
                    next(new Error("City already exists!("+ newCity + ")"));
                }

                
            }
        });
    } else {
        res.statusCode = 400;
        next(new Error("Empty Body / Invalid Body : FORMAT => { 'name' : <INPUT_CITY_ID> }"));
    }
    
});


// ROUTE DELETE CITY => Delete the city specified in URL
app.delete('/city/:id', (req, res, next) => {
    fs.readFile(fileName, 'utf8', (err, data) => {
        if(err){
            console.error(err);
            res.statusCode = 500;
            next(new Error("Can't find cities' file!"));
        } else {
            const myData = JSON.parse(data);
            const id = req.params.id;
            let index = -1;
            for (var i = 0; i < myData['cities'].length; i++) {
                if (myData['cities'][i]['id'] === id){
                    index = i;
                }
            }
            if(index != -1){
                myData['cities'].splice(index,1)
                fs.writeFile(fileName,JSON.stringify(myData), err =>{ res.statusCode = 500;});         
                res.status(200).send("<h1>City ((<i>"+ id + "</i>) deleted!</h1> ");
                console.log("DELETE : " + id);
            } else {
                res.statusCode = 404;
                next(new Error("Can't find the city specified! ("+ id +")"));
            }
        }
    });
});


app.use(function (req, res, next){
    res.status(404).send("<h1>Page not founded!</h1>");
});


app.listen(port, () => console.log(`Server running at port ${port}`));